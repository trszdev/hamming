// browser javascript (v8?)

function encode(query){
    if (query.match(/[^01]/) || query.length!=4) 
        throw 'Please input four bits';
    query += query[0] ^ query[1] ^ query[2];
    query += query[1] ^ query[2] ^ query[3];
    query += query[0] ^ query[1] ^ query[3];
    query += bitsum(query);
    return query;
}


function decode(query){ 
    if (query.match(/[^01]/) || query.length!=8) 
        throw 'Please input eight bits';
    var checks = [
        query[4] ^ (query[0]^query[1]^query[2]),
        query[5] ^ (query[1]^query[2]^query[3]),
        query[6] ^ (query[0]^query[1]^query[3]),
    ];
    var sumchecks = sum(checks);
    var fixed = query.split('');
    fixed.pop();
    var pos = -1;
    if (sumchecks!=0) 
    {
        if (sumchecks==2) {
            pos = (checks[0]+checks[1])==2 ? 2 :
                (checks[1]+checks[2])==2 ? 3 : 0;
        }
        else if (sumchecks==3) pos = 1;
        else pos = checks.indexOf(1)+4;
        fixed[pos] = query[pos] ^ 1;
    }
    else if (bitsum(fixed)!=query[7]) pos = 7;
    if (bitsum(fixed)!=query[7] && sumchecks!=0) 
        throw 'Two mistakes found, cant exactly correct them';
    fixed = fixed.join('') + query[7];
    return pos==-1 ? [0, fixed.substr(0,4)] :
        [1, fixed.substr(0,4), fixed.substring(0,pos)+'*'+fixed.substring(pos+1)]
}


bitsum = a=>sum(a)%2;
function sum(array){
    var sum = 0;
    var i = array.length; 
    while (i--) sum += array[i]-0;
    return sum;
}


function encodeButtonPressed(){
    try {
        var bits = encode($('#bitsEncode').val());
        $('#bitsDecode').val(bits);
        info('Encoded bits placed into decode field');
    } catch(e) {
        error(e);
    }
}


function decodeButtonPressed(){
    try {
        var result = decode($('#bitsDecode').val());
        //$('#bitsEncode').val(result[1]);
        if (result[0]==1)
            info('Decoded as <b>'+result[1]+'</b><br>Possible error: <b>'+result[2]+'</b>');
        else if (result[0]==0) 
            success('No errors found, decoded as <b>'+result[1]+'</b>');
    } catch(e) {
        error(e);
    }
}
     


function showAlert(title, message, type){
    $('#alert').attr('class', 'alert alert-dismissable alert-'+type);
    $('#alert').html('<button type="button" class="close" onclick="hideAlert()" aria-hidden="true">×</button>'+
        '<h4><strong>'+title+'</strong></h4>'+message);
    $('#alert').show();
}
     

function randomBitsString(bitsnum){
    var rnd = Math.floor(Math.random() * ((1<<bitsnum++)-1)); 
    var result = rnd.toString(2);
    return Array(bitsnum-result.length).join(0) + result;
}
     

function fillRandomBits(){
    $('#bitsDecode').val(randomBitsString(8));
    $('#bitsEncode').val(randomBitsString(4));
}
        

warn = (message, title)=>showAlert(title||'Warning', message, 'warning');
error = (message, title)=>showAlert(title||'Error', message, 'danger');
info = (message, title)=>showAlert(title||'Info', message, 'info');
success = (message, title)=>showAlert(title||'Success', message, 'success');
hideAlert = ()=>$('#alert').hide();
